
import sys
import os
import time
import qrcode
from PIL import Image


import RPi.GPIO as GPIO

if os.environ.get('DISPLAY','') == '':
    print('no display found. Using :0.0')
    os.environ.__setitem__('DISPLAY', ':0.0')

print("Hello Qwello")

# Data to be encoded
data = 'QwelloIsTheFutureOfCharging'

print("Generate QR...")
img = qrcode.make(data)
img.save('MyQRCode1.png')
print("OK")

# im = Image.open('MyQRCode1.png')
# im.show()
# time.sleep(1)
# im.close()


BUTTON = 24
try:

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    while (GPIO.input(BUTTON) == GPIO.HIGH):
        time.sleep(0.1)

    print("SHOW QR:")
    with Image.open('MyQRCode1.png') as im:
        im.show()

        print("Waiting button...")

        time.sleep(4)

        im.close()
finally:
    GPIO.cleanup()



# def button_changed(button):
#     if GPIO.input(button) == GPIO.LOW:
#         print("Button pressed.")
#         GPIO.output(RED_LED, GPIO.HIGH)
#     else:
#         print("Button released.")
#         GPIO.output(RED_LED, GPIO.LOW)





#GPIO.add_event_detect(BUTTON, GPIO.BOTH, callback=button_changed, bouncetime=10)

